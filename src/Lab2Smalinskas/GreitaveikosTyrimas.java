package Lab2Smalinskas;

import studijosKTU.*;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

public class GreitaveikosTyrimas {   
    ArrayList<Integer> skaiciaiArray = new ArrayList<Integer>();
    LinkedList<Integer> skaiciaiList = new LinkedList<Integer>();
    Random ag = new Random(); // atsitiktiniu generatorius
    
    int[] tiriamiKiekiai = {200_000, 250_000, 500_000, 1000_000};
    
    public void generuotiSkaicius(int elementuKiekis){        
        for(int i = 0; i < elementuKiekis; i++)
            skaiciaiArray.add(1 + ag.nextInt(100000));
        
        skaiciaiList.clear();
        for(int skaicius : skaiciaiArray)
            skaiciaiList.add(skaicius);
    }
    
    void pirmasTyrimas(int elementuKiekis){
        generuotiSkaicius(elementuKiekis);
        double kubu;
        
        long t1 = System.nanoTime();       
        
        for(int skaicius : skaiciaiArray)
            kubu = skaicius * skaicius;
        
        long t2 = System.nanoTime();
        
        for(int skaicius : skaiciaiArray)
            kubu = Math.pow(skaicius, 2);
        
        long t3 = System.nanoTime();
        
        Ks.ouf("%7d %7.4f %7.4f \n", elementuKiekis,
                (t2-t1)/1e9, (t3-t2)/1e9);
    }
    
    void antrasTyrimas(int elementuKiekis){
        generuotiSkaicius(elementuKiekis);
        
        long t1 = System.nanoTime();
        
        skaiciaiArray.contains(10000);
        
        long t2 = System.nanoTime();
        
        skaiciaiList.contains(10000);
        
        long t3 = System.nanoTime();
        
        Ks.ouf("%7d %7.4f %7.4f \n", elementuKiekis,
                (t2-t1)/1e9, (t3-t2)/1e9);
    }
    
    
    
    public void tyrimuVykdymas(){
        System.out.println("Kiekis  x*x  Math.pow(x,2)");
        for(int kiekis : tiriamiKiekiai)
            pirmasTyrimas(kiekis);
        
        System.out.println("Kiekis  ArrayList  LinkedList");
        for(int kiekis : tiriamiKiekiai)
            antrasTyrimas(kiekis);
    }
    
    public static void main(String[] args){
        long memTotal = Runtime.getRuntime().totalMemory();
        Ks.oun("memTotal= "+memTotal);
        new GreitaveikosTyrimas().tyrimuVykdymas();
   }   
}
