package Lab2Smalinskas;

import studijosKTU.*;

public class ZaidejuTurgus {
    public ListKTUx<Zaidejas> zaidejai = new ListKTUx(new Zaidejas());
    private static Zaidejas bazinisEgzm = new Zaidejas();
    
    public ListKTUx<Zaidejas> atrinktiZaidejusNesenesniusNei(int metuRiba){
        ListKTUx atrinktiZaidejai = new ListKTUx(bazinisEgzm);
        
        for (Zaidejas zaidejas : zaidejai)
            if(zaidejas.getMetai() <= metuRiba)
                atrinktiZaidejai.add(zaidejas);
        
        return atrinktiZaidejai;
    }
    public ListKTUx<Zaidejas> atrinktiZaidejusNeJaunesniusNei(int metuRiba){
        ListKTUx<Zaidejas> atrinktiZaidejai = new ListKTUx(bazinisEgzm);
        
        for(Zaidejas zaidejas : zaidejai)
            if(zaidejas.getMetai() >= metuRiba)
                atrinktiZaidejai.add(zaidejas);
        
        return atrinktiZaidejai;
    }
    public ListKTUx<Zaidejas> atrinktiZaidejusPagalPozicija(String pozicija){
        ListKTUx<Zaidejas> atrinktiZaidejai = new ListKTUx(bazinisEgzm);
        
        for (Zaidejas zaidejas : zaidejai)
            if(zaidejas.getPozicija().compareTo(pozicija) == 0)
                atrinktiZaidejai.add(zaidejas);
        
        return atrinktiZaidejai;
    }  
}
