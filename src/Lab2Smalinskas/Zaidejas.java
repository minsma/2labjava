package Lab2Smalinskas;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Comparator;
import studijosKTU.*;
import java.util.Scanner;

public class Zaidejas implements KTUable<Zaidejas>{
    
    final static private int priimtinuMetuRibaPradzia = 16;
    final static private int priimtinuMetuRibaPabaiga = 40;
    
    private String vardas;
    private String pavarde;
    private String pozicija;
    private String komanda;
    private int metai;
    
    public Zaidejas(){
    }
    
    public Zaidejas(String vardas, String pavarde, String pozicija,
                    String komanda, int metai){
        this.vardas = vardas;
        this.pavarde = pavarde;
        this.pozicija = pozicija;
        this.komanda = komanda;
        this.metai = metai;
    }
    
    public Zaidejas(String duomenuEilute){
        this.parse(duomenuEilute);
    }
    
    @Override
    public Zaidejas create(String duomenuEilute) {
        Zaidejas a = new Zaidejas();
        a.parse(duomenuEilute);
        return a;
    }
    
    @Override
    public final void parse(String duomenuEilute) {
        try{
            Scanner scanner = new Scanner(duomenuEilute);
            vardas = scanner.next();
            pavarde = scanner.next();
            pozicija = scanner.next();
            komanda = scanner.next();
            metai = scanner.nextInt();
        } catch (InputMismatchException e){
            Ks.ern("Blogas duomenų formatas apie auto -> " + duomenuEilute);
        } catch (NoSuchElementException e){
            Ks.ern("Trūksta duomenų apie auto -> " + duomenuEilute);
        }
    }
    
    @Override
    public String validate() {
        String klaidosTipas = "";
        if(metai < priimtinuMetuRibaPradzia || metai > priimtinuMetuRibaPabaiga){
            klaidosTipas = "Netinkami zaidejo metai turi buti [" +
                    priimtinuMetuRibaPradzia + ":" + priimtinuMetuRibaPabaiga + 
                    "]";
        }
        return klaidosTipas;
    }
    
    @Override
    public int compareTo(Zaidejas a) { 
        int metai = a.getMetai();
        
        if(metai < a.getMetai())
            return -1;
        if(metai > a.getMetai())
            return 1;
              
        return 0;
    }
    
    @Override
    public String toString(){
        return String.format("%-20s %-25s %-10s %-20s %3d %s", 
                             vardas, pavarde, pozicija, komanda, metai, 
                             validate());
    }
    
    public int getMetai(){
        return metai;
    }
    
    public String getVardas(){
        return vardas;
    }
    
    public String getPavarde(){
        return pavarde;
    }
    
    public String getPozicija(){
        return pozicija;
    }
    
    public String getKomanda(){
        return komanda;
    }
    
    public final static Comparator<Zaidejas> pagalVardaPavarde = 
            (Zaidejas pirmas, Zaidejas antras) -> {
                int lyginimas = pirmas.getVardas().compareTo(antras.getVardas());
                
                if(lyginimas != 0)
                    return lyginimas;
                
                return pirmas.getPavarde().compareTo(antras.getPavarde());
    };
    
    public final static Comparator<Zaidejas> pagalMetus =
            (Zaidejas pirmas, Zaidejas antras) -> {
                if(pirmas.getMetai() > antras.getMetai())
                    return 1;
                if(pirmas.getMetai() < antras.getMetai())
                    return -1;
                
                return 0;
    };
    
    public final static Comparator<Zaidejas> pagalKomanda = 
            (Zaidejas pirmas, Zaidejas antras) -> {
                int lyginimas = pirmas.getKomanda().compareTo(antras.getKomanda());
                
                if(lyginimas != 0)
                    return lyginimas;
                
                return 0;
    };
    
    public final static Comparator<Zaidejas> pagalPozicija = 
            (Zaidejas pirmas, Zaidejas antras) -> {
                int lyginimas = pirmas.getPozicija().compareTo(antras.getPozicija());
                
                if(lyginimas != 0)
                    return lyginimas;
                
                return 0;
    };
    
    @Override
    public boolean equals(Object obj){
        Zaidejas kitas = (Zaidejas) obj;
        
        if( kitas.vardas.compareTo(this.vardas) == 0 && 
            kitas.pavarde.compareTo(this.pavarde) == 0 &&
            kitas.pozicija.compareTo(this.pozicija) == 0 &&
            kitas.komanda.compareTo(this.komanda) == 0 &&
            kitas.metai == this.metai)
                return true;
        
        return false;
    }
}
