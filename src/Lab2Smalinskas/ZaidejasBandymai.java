package Lab2Smalinskas;

import studijosKTU.*;

public class ZaidejasBandymai {
    ListKTUx<Zaidejas> zaidejai = new ListKTUx(new Zaidejas());
    
    void metodoParinkimas(){
        tikrintiAtskirusZaidejus();
        formuotiZaidejuSarasa();
        perziuretiSarasa();
        papildytiSarasa();
        patikrintiTurgausApskaita();
        patikrintiRikiavima();
        patikrintiMetodus();
    }
    
    void tikrintiAtskirusZaidejus(){
        Zaidejas pirmas = new Zaidejas("Mantas", "Kalnietis", "Gynejas",
                                       "Armani",  31);
        Zaidejas antras = new Zaidejas("Mindaugas", "Kuzminskas", "Puolejas",
                                       "Knicks", 27);
        Zaidejas trecias = new Zaidejas("Jonas", "Valanciunas", "Centras",
                                        "Raptors", 25);
        
        Zaidejas kitas = new Zaidejas();
        Zaidejas kitas2 = new Zaidejas();
        kitas.parse("Paulius Jankunas Puolejas Zalgiris 33");
        kitas2.parse("Lukas Lekavicius Gynejas Panathinaikos 23");
        
        Ks.oun(pirmas);
        Ks.oun(antras);
        Ks.oun(trecias);
        
        Ks.oun("Pirmu 3 zaideju metu vidurkis= " +
                (pirmas.getMetai() + antras.getMetai() + 
                 trecias.getMetai())/3);
        
        Ks.oun(kitas);
        Ks.oun(kitas2);
        
        Ks.oun("Likusiuju 2 zaideju metu vidurkis= " +
                (kitas.getMetai() + kitas2.getMetai())/2);
    }
    
    void formuotiZaidejuSarasa(){
        Zaidejas pirmas = new Zaidejas("Mantas", "Kalnietis", "Gynejas",
                                       "Armani",  31);
        Zaidejas antras = new Zaidejas("Mindaugas", "Kuzminskas", "Puolejas",
                                       "Knicks", 27);
        Zaidejas trecias = new Zaidejas("Jonas", "Valanciunas", "Centras",
                                        "Raptors", 25);
        
        zaidejai.add(pirmas);
        zaidejai.add(antras);
        zaidejai.add(trecias);
        
        zaidejai.println("Pirmi 3 zaidejai");
        
        zaidejai.add("Paulius Jankunas Puolejas Zalgiris 33");
        zaidejai.add("Lukas Lekavicius Gynejas Panathinaikos 23");
        
        zaidejai.println("Visi 5 zaidejai");
    }
    
    void perziuretiSarasa(){
        int sk = 0;
        for(Zaidejas z: zaidejai)
            if(z.getKomanda().compareTo("Zalgiris") == 0)
                sk++;
        
        Ks.oun("Zalgirio zaideju yra = " + sk);
    }
    
    void papildytiSarasa(){
        zaidejai.add(new Zaidejas("Jonas Valanciunas Centras Raptors 25"));
        zaidejai.add("Paulius Jankunas Puolejas Zalgiris 33");
        zaidejai.add("Lukas Lekavicius Gynejas Panathinaikos 23");
        zaidejai.add("Mantas Kalnietis Gynejas Armani 31");
        zaidejai.add("Mindaugas Kuzminskas Puolejas Knicks 27");
        
        zaidejai.println("Testuojamu zaideju sarasas");
        zaidejai.save("sarasas.txt");
    }
    
    void patikrintiTurgausApskaita(){
        ZaidejuTurgus zTurgus = new ZaidejuTurgus();
        
        zTurgus.zaidejai.load("sarasas.txt");
        zTurgus.zaidejai.println("Bandomasis rinkinys");
        
        zaidejai = zTurgus.atrinktiZaidejusNeJaunesniusNei(30);
        zaidejai.println("Ne jaunesni zaidejai nei 30 metu");
        
        zaidejai = zTurgus.atrinktiZaidejusNesenesniusNei(30);
        zaidejai.println("Ne senesni zaidejai nei 30 metu");
        
        zaidejai = zTurgus.atrinktiZaidejusPagalPozicija("Gynejas");
        zaidejai.println("Gynejai");
        
        zaidejai = zTurgus.atrinktiZaidejusPagalPozicija("Puolejas");
        zaidejai.println("Puolejai");
        
        zaidejai = zTurgus.atrinktiZaidejusPagalPozicija("Centras");
        zaidejai.println("Centrai");
    }
    
    void patikrintiRikiavima(){
        ZaidejuTurgus zTurgus = new ZaidejuTurgus();
        
        zTurgus.zaidejai.load("sarasas.txt");
        zTurgus.zaidejai.println("Bandomasis rinkinys");
         
        zTurgus.zaidejai.sortBuble(Zaidejas.pagalVardaPavarde);
        zTurgus.zaidejai.println("Zaideju rinkinys isrikiuotas pagal varda ir "
                                 + "pavarde");
        
        zTurgus.zaidejai.sortBuble(Zaidejas.pagalKomanda);
        zTurgus.zaidejai.println("Zaideju rinkinys isrikiuotas pagal komandos "
                                 + "pavadinima");
        
        zTurgus.zaidejai.sortBuble(Zaidejas.pagalMetus);
        zTurgus.zaidejai.println("Zaideju rinkinys isrikiuotas pagal zaidejo "
                                 + "metus ");
        
        zTurgus.zaidejai.sortBuble(Zaidejas.pagalPozicija);
        zTurgus.zaidejai.println("Zaideju rinkinys isrikiuotas pagal zaidejo "
                                 + "pozicija");
    }
    
    void patikrintiMetodus(){
        ZaidejuTurgus zTurgus = new ZaidejuTurgus();
        
        zTurgus.zaidejai.load("sarasas.txt");
        zTurgus.zaidejai.println("Bandomasis rinkinys");
        
        zTurgus.zaidejai.add(3, new Zaidejas("Arturas Milaknis "
                                             + "Gynejas Zalgiris 31"));
        zTurgus.zaidejai.println("Rinkinys pridejus zaideja i nurodyta vieta");
        
        zTurgus.zaidejai.set(2, new Zaidejas("Arturas Gudaitis Centras Armani 24"));
        zTurgus.zaidejai.println("Rinkinys pakeitus zaidejo duomenis nurodytoje"
                                 + " vietoje");
        
        zTurgus.zaidejai.remove(1);
        zTurgus.zaidejai.println("Rinkinys pasalinus nurodyta zaideja pagal "
                                 + "indeksa");
        
        zTurgus.zaidejai.addLast(new Zaidejas("Jonas Maciulis Puolejas "
                                 + "Real 32"));
        
        zTurgus.zaidejai.println("Rinkinys pridejus nurodyta zaideja i paskutine "
                                 + "rinkinio vieta");
        
        Zaidejas zaid1 = new Zaidejas("Arturas Gudaitis Centras Armani 24");
        Zaidejas zaid2 = new Zaidejas("Arturas Milaknis Gynejas Zalgiris 31");
        
        zTurgus.zaidejai.remove(zaid1);
        zTurgus.zaidejai.remove(zaid2);
        zTurgus.zaidejai.println("Rinkinys su pasalintu nurodytu objektu");
        
        ZaidejuTurgus mTurgus = new ZaidejuTurgus();
        
        ListKTUx<Zaidejas> zaidej = new ListKTUx(new Zaidejas());
        
        zaidej.add(new Zaidejas("Mantas", "Kalnietis", "Gynejas", "Armani",  31));
        zaidej.add(new Zaidejas("Mindaugas", "Kuzminskas", "Puolejas", "Knicks", 27));
        zaidej.add(new Zaidejas("Jonas", "Valanciunas", "Centras", "Raptors", 25));
        
        zTurgus.zaidejai.removeAll(zaidej);
        zTurgus.zaidejai.println("Isvalytas sarasas");
        
        //zTurgus.zaidejai.addAll(zaidej);
        //zTurgus.zaidejai.println("Prideti zaidejai");
    }
    
    public static void main(String... args){
        new ZaidejasBandymai().metodoParinkimas();
    }
}
