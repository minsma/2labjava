# 2 Duomenų Struktūrų su Java Labaratorinis darbas #

Individuali klasių konstravimo dalis:  

1.	Pagal duotą Automobilio klasės pavyzdį sukurti individualiai pasirinktas elemento klases (4-5 komponentai), tenkinančias KTUable interfeisą; programinį kodą rašyti į individualų paketą Lab2Pavarde.  
2.	Patikrinti individualios klasės veikimą testo klasės pagalba;  
3.	Sudaryti individualių elementų apskaitos klasę, kurioje būtų elementų peržiūra ir jų atranka pagal įvairius kriterijus;  
4.	Sudaryti elementų apskaitos klasės demonstracinius metodus;  
5.	Realizuoti ListKTU metodus add(int k, Data data), set(int k, Data data), remove(int k);   
6.	Realizuoti individualiai nurodytus metodus:  
void addLast(e), int indexOf(Object o), boolean containsAll(ListKTU<?> c)  
7.	Atliekamas individualiai nurodytų metodų greitaveikos tyrimas:  
Math.pow(x, 2) <-> x*x  
ArrayList<Integer> <-> LinkedList<Integer> metodas contains(Object o)  
8.	Sunaudojamos atminties kiekio įvertinimas.  
  
Individualiai pasirenkamų duomenų tipai yra suderinami su laboratorinių darbų dėstytoju, galimi pavyzdžiai:  
prekės iš didmeninės ir mažmeninės prekybos asortimento, elektronikos komponentai, kompiuteriai, knygos, multimedijos kūriniai, kelionės, sporto varžybų ir dalyvių duomenys ir kitokie elementai, turintys po 4-5 juos apibūdinančias charakteristikas.
 
